# DYTURBOWZ

to do
- [ ] aggiornare plot con nuovo high mass CC risommato - Ale
- [ ] make plots for LHCB yZ 7TeV - Ale (Simone to send resummed)
- [ ] Girare FO e risommato per ATLAS 7TeV W+ e W-
- [ ] Girare FO e risommato per D0 A_l 

About check asked by Stefano

- Run with smaller xqtcut , xqtcut = 0.0008
    - [ ]  zypeak mass CC/CF - ALE 
    - [ ]  zyhigh mass CC/CF - Simone
    - [ ]  zylow mass CC - Simone

Run resummation uncertainty
- Run for zypeak mass CC
    - [ ] muRes variation
    - [ ] resummation dumping
    - [ ] g1 parameter variation

# macro to calculate kfactor for cross section (the procedure should be similar for every file)
import pandas as pd
import numpy as np
import time, copy
import matplotlib.pyplot as plt
import argparse

# define envelopes columns
def envelopeColumns(df):
    df = df.assign(envlope_min=lambda d: d[['sig1', 'sig2', 'sig3', 'sig4', 'sig5', 'sig6']].min(1))
    df = df.assign(envlope_max=lambda d: d[['sig1', 'sig2', 'sig3', 'sig4', 'sig5', 'sig6']].max(1))
    return df

def propErrDiv(a, ea, b, eb):
    '''
    propagate stat error after division
    '''
    div = a/b
    ea_rel = ea/a
    eb_rel = eb/b
    err_prop = div * (ea_rel**2 + eb_rel**2)**0.5
    return err_prop

def arguments(raw_args):
    #parsing argument
    parser = argparse.ArgumentParser(description='Calculate KFactor as NNLO/NLO')
    parser.add_argument('-nnlo', '--nnlo', help = 'NNLO NLOJEt data file')
    parser.add_argument('-nlog', '--nlogrid', help = 'NLO data file from the grid (fitted result file)')
    parser.add_argument("-ew","--ew", help="NLO electroweak kfactor")
    parser.add_argument("-mul","--multipl", help="The electroweak kfactor are taken into account multiplicatively, (the default is the additive)", action = "store_true")
    parser.add_argument('-s', '--scale', help = 'scale the nnlo data by this factor', type = float, default=1.)
    parser.add_argument('-bn', '--binnorm', help = 'scale nnlo prediction by the bin width', action = 'store_true')
    parser.add_argument('-o', '--oname', help = 'output file name', default= 'kf.txt')
    args = parser.parse_args(raw_args)
    return args


# read the file of theo. prediction and convert it to pandas df
# file format:
# ylow ymid yup xsec err

def main(raw_args = None):
    args = arguments(raw_args)
    nnlo_np = np.loadtxt(args.nnlo) #convert.readBinCC(args.nnlojet) # NNLO from NNLOJET

    print("\n NNLO")
    print(nnlo_np)

    # read the EW kfactor file
    nlo_ew_kf = None
    nlo_ew_kf_np = None
    if (args.ew): # if the data file is specified then open it
        nlo_ew_kf = np.loadtxt(args.ew)


    # read and convert to np
    # the column with thorig is 6
    f = open(args.nlogrid, 'r')
    read = f.read()
    read = read.replace('/0.00000E+00', '')# replace this terms, otherwise gives problems with numpy
    f = open('input_tmp.txt', 'w')
    f.write(read)
    f.close()
    fittedresults_np = np.loadtxt('input_tmp.txt', skiprows=5)
    print("NLO from GRID")
    print(fittedresults_np[:,6])
    
    # calculate the NNLO QCD, NLO EW term (additive EW)
    nnloqcd_nloew_np = copy.deepcopy(nnlo_np)
        
    if (args.binnorm):
        nnloqcd_nloew_np[:,3] /= (nnloqcd_nloew_np[:,2]-nnloqcd_nloew_np[:,0])
        
    if(args.ew): # if the ew kf file is specified calculate the KF including the EW KF
        if(args.multipl): # multiply by the EW KF
            nnloqcd_nloew_np[:,3] = nnlo_np[:,3] * nlo_ew_kf_np[:,2]
            #nnloqcd_nloew_np[:,4] = nnloqcd_nloew_np[:,3]*((nnlo_np[]/nnlo_np['sig0'])**2 + (nlo_ew_kf_np['e_stat']/nlo_ew_kf_np['kf'])**2)**0.5
            #else: # use the EW kfactor in additive way
            #nnloqcd_nloew_np['sigma'] = nnlo_np['sig0'] * (1+(nlo_ew_kf_np['kf']-1)/kfactor_lojet['kf'])
            #nnloqcd_nloew_np['e_stat'] = ((1+(nlo_ew_kf_np['kf']-1)/kfactor_lojet['kf'])**2 * nnlo_np['sigE0']**2 +
            #(nlo_ew_kf_np['kf']/kfactor_lojet['kf'])**2 * nlo_ew_kf_np['e_stat']**2 +
            #                          (nnlo_np['sig0']*(nlo_ew_kf_np['kf']-1)/kfactor_lojet['kf']**2)**2 * kfactor_lojet['e_stat']**2)**0.5
            #else: # else consider only the nnlo qcd
            #    nnloqcd_nloew_np[:,3] =  nnlo_np[:,3]
            #    nnloqcd_nloew_np[:,4] = nnlo_np[:,4]
                

    kfactor_np=copy.deepcopy(nnlo_np)
    # Calculate the kfactor as NNLO_QCD+NLO_EW/NLO_QCD_GRID
    kfactor_np[:,3] = (nnloqcd_nloew_np[:,3]*args.scale)/(fittedresults_np[:,6])
    # set the error column
    kfactor_np[:,4]= (nnloqcd_nloew_np[:,4]*args.scale)/fittedresults_np[:,6]
#propErrDiv(nnloqcd_nloew_np[:,3]*args.scale, nnloqcd_nloew_np[:,4]*args.scale, fittedresults_np[:,6], fittedresults_np[:,8])
    
    #(nnloqcd_nloew_np[:,4]*args.scale)/fittedresults_np[:,6] # progate statistical error in the calculation of the NNLO order to the kfactor
    
    print("kfactor nnlojet/grid")
    print(kfactor_np)

    np.savetxt(args.oname, kfactor_np, fmt = "%.6f")
    
    # some check plots
    # plot xsec
    f = plt.figure()
    plt.errorbar(nnloqcd_nloew_np[:,1], 
                 nnloqcd_nloew_np[:,3]*args.scale, 
                 nnloqcd_nloew_np[:,4]*args.scale,
                 label='NNLO')
    plt.errorbar(nnloqcd_nloew_np[:,1], fittedresults_np[:,6],  fittedresults_np[:,8],label='NLO grid')
    plt.legend()
    plt.title(args.nnlo)
    plt.savefig(args.oname+"_xsection.png")

    # plot kfactor
    g = plt.figure()
    plt.errorbar(kfactor_np[:,1], kfactor_np[:,3], kfactor_np[:,4],label='NNLO(DYTURBO)/nlo_grid')
    plt.legend(loc=4)
    plt.title(args.nnlo)
    plt.savefig(args.oname+".png")
    plt.show()
    
if __name__ == '__main__':
    main()

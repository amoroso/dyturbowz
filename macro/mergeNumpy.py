#!/usr/bin/env python
'''
merge prediction terms
'''
import argparse
import copy, array
import numpy as np
import matplotlib.pyplot as plt

def propErr(ea, eb):
    return (ea**2 + eb**2)**0.5

def arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file',  help = 'list of input root files', nargs = "+")
    parser.add_argument('-n', '--name', help = 'out file name', default = 'NNLO')
    #parser.add_argument('-av', '--average', help = 'average between negative and pos values', action = 'store_true')
    parser.add_argument('-p', '--plot', help = 'plot finale results and components', action = 'store_true')
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = arguments()
    list_np = []
    for f in args.file:
        list_np.append(np.loadtxt(f))

    # init the sum array
    sum_np = copy.deepcopy(list_np[0])
    sum_np[:,2] = 0
    sum_np[:,3] = 0
    print('sum terms')
    for term in list_np:   
        print(term)
        sum_np[:,2] += term[:,2]
        # propagate the error
        sum_np[:,3] = propErr( sum_np[:,3], term[:,3])

    # insert the columun bin_mid
    sum_np = np.insert(sum_np, 1, (sum_np[:,1] + sum_np[:,0])/2, axis = 1)
    print('sum ')
    print(sum_np)
    # save the sum
    np.savetxt(args.name, sum_np, fmt = '%.5f')

    if args.plot :
        plt.errorbar(sum_np[:,1], sum_np[:,3] , sum_np[:,4], label = 'sum')
        for i, term in enumerate(list_np):
            plt.errorbar(sum_np[:,1], term[:,2] , term[:,3], label = str(i))
            
        plt.legend()
        plt.show()
            

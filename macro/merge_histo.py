'''
merge histograms from root files into one histograms and save it into a new ROOT file
to use instead of hadd to merge histograms with different bin size
the histogram binning should not overlap!
'''

import ROOT as root
import argparse, array

def arguments():
    parser = argparse.ArgumentParser('merge histograms')
    parser.add_argument('-i', '--infile', nargs = '+')
    parser.add_argument('-o', '--outfile')
    parser.add_argument('-n', '--histoname', default = 's_y')
    args = parser.parse_args()
    return args

def merger(map_list):
    # sort list according to the binning
    map_list = sorted(map_list, key = lambda  el: max(el['bin']) )
    
    # check that bin_up[i] == bin_low[i+1] 
    for i in range(0, len(map_list) -1 ):
        print('bin i ', 'bin i+1', max(map_list[i]['bin']), min(map_list[i+1]['bin']))
        if abs(max(map_list[i]['bin']) - min(map_list[i+1]['bin']))>0.00001:
            raise ValueError('Error, overlapping or no-adjacent bins')
    
    # merge
    merge_map = {'bin':map_list[0]['bin'], 'content':[], 'error':[]}
    for  m in map_list:
        # merge binning, no duplicates
        merge_map['bin'] = merge_map['bin'] + list(set(m['bin']) - set(merge_map['bin']))
        
        # merge content
        merge_map['content'] += m['content'] 
        merge_map['error'] += m['error'] 
    
    return merge_map
    

if __name__ == '__main__':
    args = arguments()
    
    h_list = []
    bin_content_map_list = []

    # loop thorugh input files
    for f in args.infile:
        # get the histograms
        rootf = root.TFile(f)
        h_list.append(rootf.Get(args.histoname))
        h_list[-1].SetDirectory(0) # detach histo from file

        # fill list of maps with binning and contents information
        bin_content_map_list.append({})
        bin_content_map_list[-1]['bin'] = []
        bin_content_map_list[-1]['content'] = []
        bin_content_map_list[-1]['error'] = []
                
        for i in range(1, h_list[-1].GetNbinsX()+1):
            bin_content_map_list[-1]['bin'].append(h_list[-1].GetXaxis().GetBinLowEdge(i))
            bin_content_map_list[-1]['content'].append(h_list[-1].GetBinContent(i))
            bin_content_map_list[-1]['error'].append(h_list[-1].GetBinError(i))
            
            if i == h_list[-1].GetNbinsX():
                bin_content_map_list[-1]['bin'].append(h_list[-1].GetXaxis().GetBinUpEdge(i))

    # merge binning
    merge_map = merger(bin_content_map_list)
    print(merge_map)
    
    # create the merging histogram
    h = root.TH1D(args.histoname, args.histoname, len(merge_map['bin'])-1, array.array('d',merge_map['bin']))
    
    for i in range(0, h.GetNbinsX()):
        h.SetBinContent(i+1, merge_map['content'][i])    

    # open out root file
    out_file = root.TFile(args.outfile, "RECREATE")
    h.Write()
    out_file.Close()

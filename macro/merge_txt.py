#!/usr/bin/env python

# merge txt file - example VJ terms run with Vegas

import numpy as np
import argparse
import matplotlib.pyplot as plt
import sys
import numpy.ma as ma
#np.set_printoptions(threshold=sys.maxsize)


def arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', nargs = '+')
    parser.add_argument('-o', '--outname', default = 'out.txt')
    #parser.add_argument('-bm', '--binmid', action = 'store_true', help = 'out format : binlow binmid binup ...')
    parser.add_argument('-ro', '--removeoutl', help =  'remove outliers', action = 'store_true')
    parser.add_argument('-ns', '--nsigma', help = 'number of sigmas for outlier removal', type = int, default = 3)
    parser.add_argument('-plot', '--plot', help = 'plot VJ distribution', action = 'store_true')
    args = parser.parse_args()
    return args


def main():
    args = arguments()
    vals = []
    errs = []
    bin_up = []
    bin_down = []
    # load the txt file with predictions
    for f in args.file:
        p = np.loadtxt(f)
        vals.append(p[:,2].tolist())
        errs.append(p[:,3].tolist())
        bin_down.append(p[:,0].tolist())
        bin_up.append(p[:,1].tolist())
    
    vnp = np.array(vals).T
    # use masked array to avoid NaN
    vnp_ma = np.ma.masked_array(vnp, np.isnan(vnp))

    bindown_np_ma = np.ma.masked_array(np.array(bin_down).T, np.isnan(vnp))
    binup_np_ma = np.ma.masked_array(np.array(bin_up).T, np.isnan(vnp))
    
    binmid_np_ma = (bindown_np_ma + binup_np_ma)/2
    print(binmid_np_ma)
    
    enp = np.array(errs).T
    enp2 = enp**2
    one_enp2 = 1./enp2
    one_enp2_ma = np.ma.masked_array(one_enp2, np.isnan(one_enp2))
    avg = np.ma.average(vnp_ma, axis = 1)
    std = np.ma.std(vnp_ma, axis = 1)
    norm_err = std/(np.sum(~np.ma.getmask(vnp_ma), axis = 1))**0.5 # standard error = sigma/sqrt(N). use np.sum to count the elements  
    print('n terms: ',np.sum(~np.ma.getmask(vnp_ma), axis = 1))

    if args.plot:
        # plot 2d distribution of the xsec xsec vs bin
        # flatten the array and plot the not(nan) values
        f = plt.figure(1)
        val_flatten = (vnp_ma.T-avg).T.flatten()[~ma.getmask(vnp_ma).flatten()] # subtract the average value
        bin_mid = binmid_np_ma.flatten()[~ma.getmask(vnp_ma).flatten()]
        plt.hist2d(val_flatten, bin_mid, [100, binmid_np_ma.shape[0]])
        plt.title('(xs-average) vs bin')
        f.show()

    if args.removeoutl:
        distance = abs((vnp.T-avg)/std).T
        print('distance.shape ', distance.shape)
        print('distance',distance)
        vnp_ma = np.ma.masked_array(vnp, np.logical_or(np.isnan(vnp), distance>args.nsigma))
        print('vnp rem outliers', vnp_ma)
        # re-compute the mean
        avg = np.ma.average(vnp_ma,axis = 1)
        std = np.ma.std(vnp_ma, axis = 1)
        norm_err = std/(np.sum(~np.ma.getmask(vnp_ma), axis = 1))**0.5 # standard error = sigma/sqrt(N). use np.sum to count the elements  
        print('n terms (after remove outliers): ',np.sum(~np.ma.getmask(vnp_ma), axis = 1))

        
        if args.plot:
            # plot 2d distribution of the xsec xsec vs bin
            # flatten the array and plot the not(nan) values
            g = plt.figure(2)
            val_flatten = (vnp_ma.T-avg).T.flatten()[~ma.getmask(vnp_ma).flatten()] # subtract the average value
            bin_mid = binmid_np_ma.flatten()[~ma.getmask(vnp_ma).flatten()]
            plt.hist2d(val_flatten, bin_mid, [100, binmid_np_ma.shape[0]])
            plt.title('(xs-average) vs bin - post remove outliers')
            g.show()


    bins = np.loadtxt(args.file[0]) # get the binning columns
    
    pred_merge = np.c_[bins[:,0],bins[:,1],avg,norm_err]
    
    pred_merge = np.delete(pred_merge, -1, axis=0)
    print(pred_merge)
    np.savetxt(args.outname, pred_merge, fmt = '%1.5f')

if __name__ == '__main__':
    main()
    raw_input()

#!/usr/bin/env python

import matplotlib.pyplot as plt
import argparse, os, sys
import numpy as np

def arguments(raw_args):
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', nargs = "+", help = "input file - format: bin_low bin_mid bin_up val err_val")
    parser.add_argument('-leg', '--leg', nargs = '+', help = 'legend entry', default = [])
    parser.add_argument('-t', '--title', help = 'plot title', default = 'DYTURBO')
    parser.add_argument('-xt', '--xtitle', help = 'xaxis title', default = '$|y_{\mu\mu}|$')
    parser.add_argument('-yt', '--ytitle', help = 'yaxis title', default = '$d\sigma / d|y_{\mu\mu}|\,\, [pb]$')
    parser.add_argument('-s', '--save', help = 'save name', default = 'plot')
    parser.add_argument('-ofold', '--outfolder', help = 'out folder. Created if it does not exsist', default = './')
    parser.add_argument('-xmax', '--xmax', help = 'maximum of xaxis', default = None, type = float)
    parser.add_argument('-norm', '--norm', help = 'normalize', action = 'store_true')
    parser.add_argument('-bn', '--binnorm', help = 'normalize by bin width', nargs = '+', default = [], type = int)
    parser.add_argument('-sc', '--scale', help = 'scale factor', nargs = '+', default = [], type = float)
    parser.add_argument('-text', '--text', help = 'add text on plot', default = None)
    parser.add_argument('-tp', '--textpos', help = 'text position, in % of user coordinate',
                        default = None, nargs = 2, type = float)
    args = parser.parse_args(raw_args)
    return args


def main(raw_args = None):
    args = arguments(raw_args)
    
    if len(args.leg) < len(args.file): # if legend entry not specified...
        i = 0
        while len(args.leg) < len(args.file):
            args.leg.append('pred '+str(i))
            i +=1

    if len(args.binnorm) < len(args.file): # if binnorm entry not specified, set to false...
        while len(args.binnorm) < len(args.file):
            print('args.binnorm.append(False)')
            args.binnorm.append(False)
            
    if len(args.scale) < len(args.file): # if scale entry not specified, set to 1
        while len(args.scale) < len(args.file):
            args.scale.append(1)

            
    fig, axs = plt.subplots(2, figsize=(8, 6), gridspec_kw={'height_ratios': [0.6, 0.4]}, sharex = True)
    fig.suptitle(args.title)
    
    ratio_to = None # np.loadtxt(args.file[0]) # ratio to the first entry    

    marker_map = {0:'o', 1:'^', 2:'s', 3:'v'}
    print(args.binnorm)
    for i, f in enumerate(args.file):
        f_np = np.loadtxt(f)

        # apply scale factor
        f_np[:,3] *= args.scale[i]
        f_np[:,4] *= args.scale[i]

        # bin width normalization
        if args.binnorm[i] == 1:
            print('binnormx')
            f_np[:,3] /= (f_np[:,2] - f_np[:,0])
            f_np[:,4] /= (f_np[:,2] - f_np[:,0])

        if args.norm == True:
            xsec_sum = f_np[:,3].sum()
            f_np[:,3] = f_np[:,3]/xsec_sum
            f_np[:,4] = f_np[:,3]/xsec_sum        

        # set the prediction to which perform the ratio 
        if i == 0:
            ratio_to = f_np
            
        axs[0].errorbar(f_np[:,1], f_np[:,3],
                        f_np[:,4],(f_np[:,2]-f_np[:,0])/2,
                        label = args.leg[i], marker = marker_map[i],linestyle = '')
        axs[1].errorbar(f_np[:,1], f_np[:,3]/ratio_to[:,3],
                        f_np[:,4]/ratio_to[:,3], (f_np[:,2]-f_np[:,0])/2,
                        marker = marker_map[i], linestyle = '')

        if args.xmax:
            axs[0].set_xlim(xmax = args.xmax)
        axs[1].set_xlabel(args.xtitle)
        axs[1].set_ylabel("Pred./"+args.leg[0])
        axs[0].set_ylabel(args.ytitle)
        axs[0].legend()

        axs[0].minorticks_on()
        axs[1].minorticks_on()

    #set grid
    #axs[0].grid()
    axs[1].grid()

        
    # write text in the upper plot
    if args.text:
        text_pos = [0.05,0.5]
        if args.textpos:
            text_pos = args.textpos
        axs[0].annotate(args.text,
                     #r'\noindent ATLAS 7TeV \\ Z $\rightarrow\ell\ell$ CC \\ $46<m_{\ell\ell}<66$ GeV',
                     xy=text_pos, xycoords='axes fraction',usetex=True, fontsize = 'large')
    #r'\noindent ATLAS 7TeV \\ Z $\rightarrow\ell\ell$'

        
    if (not os.path.exists(args.outfolder)): # if the path does not exist create it
        os.makedirs(args.outfolder)

    plt.subplots_adjust(wspace=0, hspace=0.1)
    
    plt.savefig(os.path.join(args.outfolder, args.save+'.pdf'))
    plt.savefig(os.path.join(args.outfolder, args.save+'.png'))

    plt.show()

if __name__ == '__main__':
    main()

#!/usr/bin/env python2

import ROOT as root
import argparse
import copy, array
import numpy as np

def histToNumpy(h, bin_mid = True):
    '''given the root histogram return a numpy with entries and error of the histo'''
    matrix = []
    for i in range(1, h.GetNbinsX()+1):
        bin_low = h.GetXaxis().GetBinLowEdge(i)
        bin_up = h.GetXaxis().GetBinUpEdge(i)
        bin_mid = (bin_low + bin_up)/2
        xsec = h.GetBinContent(i)
        err = h.GetBinError(i)
        line = []
        if bin_mid is True:
            line = [bin_low, bin_mid, bin_up, xsec, err]
        else:
            line = [bin_low, bin_up, xsec, err]
        matrix.append(line)
    xsec_np = np.asarray(matrix)
    return xsec_np

def numpyToHist(v, name ='s_y'):
    binning = list(v[:,0])
    binning.append(v[-1,2])
    binning = array.array('d', binning)
    h = root.TH1D(name, name, len(binning)-1, binning)
    
    for i in range(0, h.GetNbinsX()):
        h.SetBinContent(i+1, v[i,3])
        h.SetBinError(i+1, v[i,4])

    return h

def abs_y(arr):
    '''return the array calculating the absolute y value'''
    arr_n = arr[arr[:,0] < 0]
    arr_n = np.flip(arr_n, axis = 0) # invert the order of the bins
    arr_p = arr[arr[:,0] >= 0]
    
    #print('arr_n', arr_n)
    #print('arr_p', arr_p)
    
    arr_p[:,3] = (arr_n[:,3]+arr_p[:,3])/2 # mean
    arr_p[:, 4] = (arr_p[:,4]**2 + arr_n[:,4]**2)**0.5 * 0.5 # propagate the error
    return arr_p

def abs_yToMap(np_map):
    '''apply the y average operation to a map of np'''
    np_map_absy = {}
    for i in np_map:
        np_map_absy[i] = abs_y(np_map[i])
    return np_map_absy
    
def getNPmap(histo_map):
    ''' Get a map of numpy from map of histograms '''
    np_map = {}
    for i in histo_map:
        np_map[i] = histToNumpy(histo_map[i])
    return np_map

def getDYTMap(dict_list, h_sum_name = 'nnlo_fo'):
    '''read the histograms and return a map that merge them'''
    histo_map = {}
    for d in dict_list:
        f = root.TFile(os.path.join(d['file_paht'], d['file_name']))
        h = f.Get(d['h_name'])
        h.SetDirectory(0) # detach histogram from file
        histo_map[d['dict_name']] = h # add entry to the returned map
    # calculate the sum
    hsum = copy.deepcopy(list(histo_map.values())[0].Clone(h_sum_name))
    hsum.Reset() # reset the histo
    for i in histo_map:
        hsum.Add(histo_map[i])
        
    # add the sum to the map
    histo_map[h_sum_name] = hsum
    
    # now create a map of numpy elements
    histo_map_np = getNPmap(histo_map)
    
    return histo_map, histo_map_np

def arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file',  help = 'list of input root files', nargs = "+")
    parser.add_argument('-hn', '--histo', help = 'histo name to retrieve', default = 's_y')
    parser.add_argument('-n', '--name', help = 'out file name', default = 'NNLO')
    parser.add_argument('-av', '--average', help = 'average between negative and pos values', action = 'store_true')
    parser.add_argument('-bm', '--binmid', help = 'print the bin mid in the output file', action = 'store_false')
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = arguments()

    # get histograms from all the file
    h_list = []
    for f in args.file:
        print(f) 
        fr = root.TFile(f)
        h_list.append(fr.Get(args.histo))
        h_list[-1].SetDirectory(0)

    # sum up the histograms
    h_sum = copy.deepcopy(h_list[0].Clone(args.histo))
    h_sum.Reset()
    for i , h in enumerate(h_list):
        h_sum.Add(h)

    # convert to numpy
    sum_np = histToNumpy(h_sum, bin_mid = args.binmid)
    
    # average, absolute rapidity
    if args.average:
        sum_np = abs_y(sum_np)
        h_sum = numpyToHist(sum_np, h_sum.GetName())

    print("prediction")
    print(sum_np)
    
    # save to root file and txt file
    fout = root.TFile(args.name+".root", "RECREATE")
    fout.cd()
    print(h_sum)
    h_sum.Write()
    fout.Close()
    
    np.savetxt(args.name+".txt", sum_np, fmt='%10.6f')

#!/usr/bin/env python2

'''
make histogram from txt file and save to root file
'''

import ROOT as root
import argparse
import copy, array
import numpy as np
import array

def arguments():
    parser = argparse.ArgumentParser('convert .txt to histogram in root file. expected format: col1=bin_low, col2=bin_up, col3=value, col4=err')
    parser.add_argument('-txt', '--txt',  help = 'text file, expected format: col1=bin_low, col2=bin_up, col3=value, col4=err')
    parser.add_argument('-n', '--name', help = 'out file name', default = 'NNLO')
    parser.add_argument('-hn', '--histo', help = 'histo name to retrieve. default = s_y', default = 's_y')
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = arguments()

    v = np.loadtxt(args.txt)
    # get binning for the histogram
    binning = list(v[:,0])
    binning.append(v[-1,1])
    binning = array.array('d', binning)

    h = root.TH1D("s_y", "s_y", len(binning)-1, binning)

    # fill the histogram
    for i in range(0, h.GetNbinsX()):
        h.SetBinContent(i+1, v[i,2])
        h.SetBinError(i+1, v[i,3])

    # save histo to file
    f = root.TFile(args.name, "RECREATE")
    h.Write()
    f.Close

    h.Draw()
    drawing = raw_input("enter")
        
